/**
 * Represents the Background
 * @constructor
 */
function Background() {
    /**
     * @type {number} posX - Represents the current location of the Background
     */
    var posX = 0;

    /**
     * Represents the movement of the Background
     * @constructor
     */
    this.draw = function () {
        /**
         * @type {number} posX = (posX - 1) % width - Represents a movement of 1 unit to the left
         */
        posX = (posX - 1) % width;
        /**
         * Represents the General Information of the Background Position
         * @image
         */
        image(images.background, posX, 0);
    }
}

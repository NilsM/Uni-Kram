'use strict';

var images;
var mario;
var bg;
var obstacles = [];
var GROUNDLEVEL = 360;
function preload() {
	var imgBackground = loadImage("assets/newBackground.jpg");
	var imgMario = loadImage("assets/mario.jpg");
	var imgUhr = loadImage("assets/Uhr.jpg");
	var imgUmgeUhr = loadImage("assets/UmgeUhr.jpg");
	var imgNewObstacle = loadImage("assets/newobstacle.jpg");
    images = {
		background: imgBackground,
		mario: imgMario,
		uhr: imgUhr,
		umgeuhr: imgUmgeUhr,
		obstacle: imgNewObstacle

	};
    console.log("end of preload");
}
function setup() {
    createCanvas(800, 500);
    mario = new Mario();
	bg = new Background();
	for(var i=0; i<5; i++)
		obstacles.push(new Obstacle());
    console.log("end of setup");
}

function draw() {
	bg.draw();
	for(var i=0; i<obstacles.length; i++){
		obstacles[i].draw();
		if (obstacles[i].box.isCollission(mario.box)) {
			console.log("collission: " + mario.box.toString() + ", " + obstacles[i].box.toString());
			noLoop();
		}
	}
    mario.draw();
}

function keyPressed() {
  mario.jump();
}

function mousePressed() {
  loop();
}